#pragma once
#ifndef MEMBER_MARAPHON_H
#define MEMBER_MARAPHON_H
#include "constants.h"

struct date
{
    int hour;
    int minutes;
    int second;
};

struct person
{
    char first_name[MAX_STRING_SIZE];
    char middle_name[MAX_STRING_SIZE];
    char last_name[MAX_STRING_SIZE];
};

struct member_maraphone
{
    int number;
    person member;
    date start;
    date finish;
    char club[MAX_STRING_SIZE];

};

#endif
