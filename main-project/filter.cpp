#include "filter.h"
#include <cstring>
#include <iostream>

member_maraphone** filter(member_maraphone* array[], int size, bool (*check)(member_maraphone* element), int& result_size)
{
	member_maraphone** result = new member_maraphone * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_club_member(member_maraphone* element)
{
	char s2[100] = "�������";
	return strcmp(element->club, s2) == 0;


}

bool check_member_by_result(member_maraphone* element)
{
	if (element->finish.hour - element->start.hour < 2)
	{
		return 1;
	}
	return element->finish.hour - element->start.hour <= 2 && element->finish.minutes - element->start.minutes < 30;
}